# docker-tartube

A dockerized installation of [Tartube](https://github.com/axcore/tartube) which is fronted by TigerVNC for easy access to the otherwise desktop application.
